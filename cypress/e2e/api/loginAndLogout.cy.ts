import 'cypress-mochawesome-reporter/register';

describe('Login and Logout', () => {
  it("Login - Bynder Portal", () => {
    cy.login();
    cy.url().should('include', '/account/dashboard');
  });

  it("Logout - Bynder Portal", () => {
    cy.login();
    cy.url().should('include', '/account/dashboard')
    cy.get('.admin-dropdown.profile').click();
    cy.get('.fa.fa-power-off').click();
    cy.get('p.cbox_messagebox').should('have.text', 'You have successfully logged out.');
    cy.url().should('include', '/login')
  });
});



