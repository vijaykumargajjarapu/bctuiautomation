import 'cypress-mochawesome-reporter/register';


describe('Login Page - Different Scenarios', () => {

  beforeEach(() => {
    cy.visit(Cypress.env('baseUrl'));
  });
  it("Verify that error message is displayed when invalid username and password are used in login page", () => {
    cy.get('#inputEmail').type('invalidUsername');
    cy.get('#inputPassword').type('invalidPassword');
    cy.get('.login-btn.action-btn.blue.block').click();
    //Navigating to Security Check Page instead of showing error message in the screen,hence commented below lines
    //cy.get('cbox_messagebox').should('be.visible');
    //cy.get('p.cbox_messagebox').should('have.text', 'You have entered an incorrect username or password.');
  });

  it("Verify that 'Please enter your Username.' error message in login page", () => {
    cy.get('#inputPassword').type(Cypress.env('password'));
    cy.get('.login-btn.action-btn.blue.block').click();
    cy.get('p.cbox_messagebox').should('have.text', 'Please enter your Username.');
  });

  it("Verify that 'Please enter your password.' error message in login page", () => {
    cy.get('#inputEmail').type(Cypress.env('username'));
    cy.get('.login-btn.action-btn.blue.block').click();
    cy.get('p.cbox_messagebox').should('have.text', 'Please enter your password.');
  });

  it("Verify the number of supporting languages is 6", () => {
    cy.get('a[href="#"]').contains('Language').click();
    cy.get('#languageSwitch').find('li').should('have.length', 6);
    cy.get('#languageSwitch').find('li').children().find('a').each(($a) => cy.log($a.text()));
  });

  it("Verify the default Language is English (United States)", () => {
    cy.get('a[href="#"]').contains('Language').click();
    cy.get('.fa.fa-check.language-switch__selected-indicator').parent('a').contains('English (United States)');
  });

  it("Verify the logo text", () => {
    cy.visit(Cypress.env('baseUrl'));
    cy.get('.admin-bar-logo').contains('Bynder');
  });

  it("Verify the logo text", () => {
    cy.visit(Cypress.env('baseUrl'));
    cy.get('.admin-bar-logo').contains('Bynder');
  });

  it("Verify the lost password link", () => {
    cy.visit(Cypress.env('baseUrl'));
    cy.get('a[href="/user/forgotPassword/?redirectToken="]').click();
    cy.get('.info').should('be.visible');
    cy.url().should('include', 'forgotPassword/?redirectToken');
  });  

  it("Verify the support functionality", () => {
    cy.visit(Cypress.env('baseUrl'));
    cy.get('#custom-support-form-button').click();
    cy.contains('Support').should('be.visible');
  }); 

});
