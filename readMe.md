# Install Node and NPM
Windows:
https://phoenixnap.com/kb/install-node-js-npm-on-windows

Mac:
https://phoenixnap.com/kb/install-npm-mac

# How to install cypress using npm
npm install cypress --save-dev

# How to install docker
Install Docker from https://docs.docker.com/get-docker/ in windows or mac

# Pull Docker Image
docker pull cypress/included:12.4.1

# How to run using Docker:
docker run -it -v $PWD:/e2e -w /e2e cypress/included:12.4.1

# How to run locally environment wise
npx cypress run --env name=qa

# Scenarios:
# Feature: Login and Logout 

Background:

Given i launched a bynder url

# Scenario: Login

When User enter username and password

And User click on Login Button

Then Verify user is navigated to Dashboard page

# Scenario: Logout

When User enter username and password

And User click on Login Button

Then Verify user is navigated to Dashboard page

And User click on Profile button

And Click on Logout Button

Then Verify that User is logged out

#  Feature: Login Page Functionality

Background:

Given User launched the bynder portal

# Scenario Outline: Error message for invalid credentials

When User enter "<username>" and "<password>" for condition '<Description>'

And User Click on Login Button

Then Verify the "<ErrorMessage>"

Examples:
|Description                 | username     |password   | ErrorMessage                                       |

|Invalid Username & Password |invalid       |invalid    | You have entered an incorrect username or password.|

|Invalid Username            |              |PassWord!1 | Please enter your Username.                        |

|Invalid Username & Password |qa-assigment  |invalid    | Please enter your password.                        |

# Scenario: Validate number of languages in Bynder Portal

When User click on Language Button

Then Verify that Number of languages should be equal to 6

# Scenario: Verify the logo text

Then Verify that Logo text is Bynder

# Scenario: Verify the Default language

When User click on Language Button

Then Verify that default language is English (United States)

# Scenario: Verify lost password link navigation

When User click on Lost Password link

Then Verify that user is navigated to forgot password page

# Scenario: Verify the Support Functionality

When User Click on Support button

Then Verify that Support Screen is displayed