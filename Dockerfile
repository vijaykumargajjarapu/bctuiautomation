ARG CHROME_VERSION='107.0.5304.121-1'


FROM cypress/factory


WORKDIR /app
COPY . .


RUN npm install --save-dev cypress